# Textop
## Purpose
The purpose of Textop (apart from self-educational) is to support me, or anyone, in getting quite disciplined.

## Features
* Web app only, with modern GUI and responsive/mobile design.
* Ruby, Rails, and jQuery, with a PostgreSQL database.
* Combines to do list, schedule, activity/time log, and notes.
* Not a group product. Meant for organizing an individual's activity, not a team, but does offer multiple accounts and encrypted data for each instance.
* Offers categories for focusing.
* Allows user to create, edit, and delete repeated/common activities (such as "Exercise" or "Shower"). These are auto-created when the user types the same activity more than once.
* Keyboard shortcuts throughout, including a console input by pressing '/'. Console input might be faster than using the web-based GUI.
* The to do list supports subtasks, dragging and dropping


## Front page

### Function
The function of the front page, of course, is to serve as the main interface
for the app. There's no reason to go elsewhere.

### Structure
When logged out, the front page will have basic information about the app.
This can be fleshed out later and can be a brief placeholder to begin with.

When logged in, however, the front page will make it as easy as possible to
view and update everything.

### Basic design
If the app combines to do list, schedule, activity logger, and notes, we're
really talking about four apps in one. So, how *should* this work? The usual
interface *won't* be the to do list or notes, but instead the schedule, with
a place to input new activities. Presumably, when you click on the schedule,
you'll go to the schedule, which would eventually (I assume) be a
full-functioned calendar, complete with support for .ics, CalDAV, etc.
The activity logger would be positioned above that on the page, at the top of
the page, with its various tools. What tools? Probably a text box for raw
input (i.e., command line emulating, since that might be the fastest
way, in many cases) along with a drop-down or better yet, a button you tap on
which pops up a grid-like list of your most common tasks.
