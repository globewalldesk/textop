# Textop

## Name

My first attempt to create a website patterned after http://textop.org/old/

## Installation

Uses Ruby 2.5.3.

Production version uses PostgreSQL so you'll have to have that installed if you want to run it on a server. Locally the app runs SQLite.
