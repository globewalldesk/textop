require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  def setup
    @base_title = 'Textop'
  end

  test "should get root/home" do
    get root_path
    assert_response :success
    assert_select "title", "#{@base_title} | The Text Outline Project"
  end

  test "should get about" do
    get about_path
    assert_response :success
    assert_select "title", "#{@base_title} | About"
  end

  test "should get privacy" do
    get privacy_path
    assert_response :success
    assert_select "title", "#{@base_title} | Privacy"
  end

  test "should get contact" do
    get contact_path
    assert_response :success
    assert_select "title", "#{@base_title} | Contact"
  end

end
