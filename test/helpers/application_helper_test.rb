require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title, "Textop"
    assert_equal full_title("Foobar"), "Textop | Foobar"
  end
end
